#!/bin/bash

pyside6-rcc ressources/icons/icons.qrc > src/icons_rc.py
pyside6-rcc ressources/styles/styles.qrc > src/styles_rc.py
pyside6-rcc ressources/textures/textures.qrc > src/textures_rc.py

