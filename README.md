# QOsuPlayer

> This software is in development

QOsuPlayer is a music player for osu!. It can read music from an osu! path and display some stats on beatmaps.


## Install

### Dist


### For development

```
git clone https://gitlab.com/osupy/qosuplayer.git
```
## Requirement for development

 * [PySide6][pyside]
 * [osupy][osupy]
 * [pyosudb][pyosudb]


## License

GNU GPL 3.0 License
```
Copyright (C) 2021  LostPy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/gpl-3.0.html.
```

[pyside]:https://doc.qt.io/qtforpython/
[osupy]: https://gitlab.com/osupy/osupy
[pyosudb]: https://gitlab.com/osupy/pyosudb

